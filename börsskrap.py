#!/usr/bin/env python3
import mysql.connector as mariadb
import requests

from bs4 import BeautifulSoup
from datetime import date

import databasinfo as db

index = requests.get('https://trader.di.se/index.php/stocklist/index/1021?list=7126')
soppa = BeautifulSoup(index.content, 'html.parser')

udda_tabellrader = soppa.find('table', attrs={'stock-table'}).find_all('tr', 'odd')
jämna_tabellrader = soppa.find('table', attrs={'stock-table'}).find_all('tr', 'even')

tabellrader = udda_tabellrader + jämna_tabellrader

antal_kolumner = 10

aktier = []

class Aktie:
	fält = { 0: 'namn',
	         1: 'procent',
	         2: 'avslut',
	         3: 'köp',
	         4: 'sälj',
	         5: 'senast',
	         6: 'högst',
	         7: 'lägst',
	         8: 'volym',
	         9: 'tid' }

	def __init__(self, namn):
		self.data = {}
		self.namn = namn
		self.data['namn'] = namn

	def sätt(self, index, värde):
		if self.fält[index] == 'tid':
		    värde = f'{date.today()} {värde}:00'

		self.data[self.fält[index]] = värde

	def __str__(self):
		str = f'{self.fält[0]}: {self.data[self.fält[0]]}'
		for i in range(1, antal_kolumner):
			str += f', {self.fält[i]}: {self.data[self.fält[i]]}'
		return str

	def sql(self):
	    return tuple([ val.replace(',', '.').replace(' ', '') if key != 'tid' else val for (key, val) in self.data.items() ])

aktie = None
for tabellrad in tabellrader:
	kolumn = 0
	for data in tabellrad.find_all('td'):
		text = data.get_text().strip()
		if text == '':
			continue

		if kolumn == 0:
			aktie = Aktie(text)
		else:
			aktie.sätt(kolumn, text)

		kolumn += 1

	if kolumn == antal_kolumner:
		aktier.append(aktie)

anslutning = mariadb.connect(user=db.USER,
                             host=db.HOST, port=db.PORT,
                             force_ipv6=db.FORCE_IPV6,
			     password=db.PASSWORD,
                             database=db.DATABASE)

markör = anslutning.cursor()

for aktie in aktier:
	markör.execute("INSERT INTO aktier (namn, procent, avslut, kop, salj, senast, hogst, lagst, volym, tid) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)", aktie.sql())

anslutning.commit()
