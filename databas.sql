CREATE TABLE aktier (
    id INT UNSIGNED NOT NULL AUTO_INCREMENT,
    namn VARCHAR(200),
    procent FLOAT,
    avslut FLOAT,
    kop FLOAT,
    salj FLOAT,
    senast FLOAT,
    hogst FLOAT,
    lagst FLOAT,
    volym FLOAT,
    tid DATETIME,
    PRIMARY KEY (id),
    INDEX namnindex (namn)
);

